package com.latomuchmitrus.pmapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import helpers.InputValidation;
import sql.DatabaseHelp;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = LoginActivity.this;

    private ConstraintLayout constraintLayout;

    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutPassword;

    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextPassword;

    private Button buttonLogin;
    private TextView linkRegister;

    private InputValidation inputValidation;
    private DatabaseHelp databaseHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObject();
    }

    private void initViews() {
        constraintLayout = (ConstraintLayout) findViewById(R.id.constrainLayout);

        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        linkRegister = (TextView) findViewById(R.id.linkRegister);
    }

    private void initListeners() {
        buttonLogin.setOnClickListener(this);
        linkRegister.setOnClickListener(this);
    }

    private void initObject() {
        databaseHelp = new DatabaseHelp(activity);
        inputValidation = new InputValidation(activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonLogin:
                verifyFromSQL();
                break;
            case R.id.linkRegister:
                Intent intentRegister = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intentRegister);
                break;
        }
    }

    private void verifyFromSQL() {
        if(!inputValidation.isInputFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }

        if(!inputValidation.isEmailValid(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }

        if(!inputValidation.isInputFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
            return;
        }

        if(databaseHelp.checkIfExist(textInputEditTextEmail.getText().toString().trim(), textInputEditTextPassword.getText().toString().trim())) {

            //Tylko przyklad - otwiera nową klasę
            Intent intent = new Intent(activity, HelloWorldClass.class);
            intent.putExtra("EMAIL", textInputEditTextEmail.getText().toString().trim());
            emptyInputEditText();
            startActivity(intent);

//            Co ma sie zadziac po zalogowaniu. Np - HelloWorldClass
//            Intent intent = new Intent(activity, KLASA_DO_OPRACOWANIA (W ZALEZOSCI CO MA ROBIC APKA));
//            startActivity(intent);
//            emptyInputEditText();

            System.out.println("Login is working!");
        } else {
            Snackbar.make(constraintLayout, getString(R.string.error_valid_email_password), Snackbar.LENGTH_LONG).show();
        }
    }

    private void emptyInputEditText() {
        textInputEditTextEmail.setText(null);
        textInputEditTextPassword.setText(null);
    }
}
