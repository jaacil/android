package com.latomuchmitrus.pmapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import helpers.InputValidation;
import sql.DatabaseHelp;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = RegisterActivity.this;

    private ConstraintLayout constraintLayout;

    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmationPassword;

    private TextInputEditText textInputEditTextName;
    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextPassword;
    private TextInputEditText textInputEditConfirmationPassword;

    private Button buttonRegister;
    private TextView textViewLogin;

    private InputValidation inputValidation;
    private DatabaseHelp databaseHelp;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    public void initViews() {

        constraintLayout = (ConstraintLayout) findViewById(R.id.constrainLayout);

        textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);;
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);;
        textInputLayoutConfirmationPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);

        textInputEditTextName = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
        textInputEditConfirmationPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        textViewLogin = (TextView) findViewById(R.id.textViewLogin);

    }

    public void initListeners() {
        buttonRegister.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);
    }

    public void initObjects() {
        inputValidation = new InputValidation(activity);
        databaseHelp = new DatabaseHelp(activity);
        user = new User();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonRegister:
                sendDataToSQL();
                break;
            case R.id.textViewLogin:
                finish();
                break;
        }
    }

    private void sendDataToSQL() {
        if(!inputValidation.isInputFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
            return;
        }
        if(!inputValidation.isInputFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }
        if(!inputValidation.isEmailValid(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email))) {
            return;
        }
        if(inputValidation.isInputFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
            return;
        }
        if(!inputValidation.isInputMatches(textInputEditTextPassword, textInputEditConfirmationPassword, textInputLayoutConfirmationPassword, getString(R.string.error_password_match))) {
            return;
        }

        if(!databaseHelp.checkIfExist(textInputEditTextEmail.getText().toString().trim())) {
            user.setName(textInputEditTextName.getText().toString().trim());
            user.setEmail(textInputEditTextEmail.getText().toString().trim());
            user.setPassword(textInputEditTextPassword.getText().toString().trim());

            databaseHelp.addUser(user);

            Snackbar.make(constraintLayout, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
            emptyInputs();
        } else {
            Snackbar.make(constraintLayout, getString(R.string.error_email_exists), Snackbar.LENGTH_LONG).show();
        }
    }

    public void emptyInputs() {
        textInputEditTextName.setText(null);
        textInputEditTextEmail.setText(null);
        textInputEditTextPassword.setText(null);
        textInputEditConfirmationPassword.setText(null);
    }

}

