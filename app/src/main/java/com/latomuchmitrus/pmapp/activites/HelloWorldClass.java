package com.latomuchmitrus.pmapp.activites;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.latomuchmitrus.pmapp.R;
import com.latomuchmitrus.pmapp.model.User;
import com.latomuchmitrus.pmapp.sql.DatabaseHelp;

import java.util.ArrayList;
import java.util.List;

public class HelloWorldClass extends AppCompatActivity {

    private AppCompatActivity activity = HelloWorldClass.this;

    private TextView textViewName;

    private List<User> listUsers;

    private DatabaseHelp databaseHelp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hallo);
        getSupportActionBar().setTitle("Hi Hi Hello");
        initViews();
        initObjects();
    }

    public void initViews() {
        textViewName = (TextView) findViewById(R.id.textViewName);
    }

    public void initObjects() {

        listUsers = new ArrayList<>();

        String email = getIntent().getStringExtra("EMAIL");
        textViewName.setText(email);

        databaseHelp = new DatabaseHelp(activity);
        getDataFromSQL();
    }

    private void getDataFromSQL() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                listUsers.clear();
                listUsers.addAll(databaseHelp.getAllUsers());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

}
