package com.latomuchmitrus.pmapp.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.latomuchmitrus.pmapp.model.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelp extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "pmapp.db"; //nazwa bazy danych (aplikacja przetwarzanie mobilne)
    private static final String USERS_TABLE = "users";

    private static final String USER_COLUMN_ID = "userID";
    private static final String USER_COLUMN_NAME = "userName";
    private static final String USER_COLUMN_EMAIL = "email";
    private static final String USER_COLUMN_PASSWORD = "password";


    private String USER_CREATE_TABLE = "CREATE TABLE " + USERS_TABLE + "(" + USER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            USER_COLUMN_NAME + " VARCHAR," + USER_COLUMN_EMAIL + " VARCHAR," + USER_COLUMN_PASSWORD + " VARCHAR" + ")";

    private String USER_DROP_TABLE = "DROP TABLE IF EXISTS " + USERS_TABLE;

    public DatabaseHelp(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(USER_DROP_TABLE);
        onCreate(db);
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_COLUMN_NAME, user.getName());
        values.put(USER_COLUMN_EMAIL, user.getEmail());
        values.put(USER_COLUMN_PASSWORD, user.getPassword());

        db.insert(USERS_TABLE, null, values);
        db.close();
    }

    public List<User> getAllUsers() {
        String columns[] = {
                USER_COLUMN_ID,
                USER_COLUMN_NAME,
                USER_COLUMN_EMAIL,
                USER_COLUMN_PASSWORD
        };

        String sortOrder = USER_COLUMN_NAME + " ASC";
        List<User> usersList= new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(USERS_TABLE,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if(cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(USER_COLUMN_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(USER_COLUMN_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(USER_COLUMN_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(USER_COLUMN_PASSWORD)));

                usersList.add(user);
            } while (cursor.moveToNext());
        }
        return usersList;
    }

    public void updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_COLUMN_NAME, user.getName());
        values.put(USER_COLUMN_EMAIL, user.getEmail());
        values.put(USER_COLUMN_PASSWORD, user.getPassword());

        db.update(USERS_TABLE, values, USER_COLUMN_ID + " = ?", new String[] {String.valueOf(user.getId())});
        db.close();
    }

    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(USERS_TABLE, USER_COLUMN_ID + " = ?", new String[] {String.valueOf(user.getId())});
        db.close();
    }

    public boolean checkIfExist(String email) {
        String[] columns = {
                USER_COLUMN_ID};

        SQLiteDatabase db = this.getReadableDatabase();

        String select = USER_COLUMN_EMAIL + " = ?";
        String[] selectArg = {email};

        Cursor cursor = db.query(USERS_TABLE,
                columns,
                select,
                selectArg,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        return cursorCount > 0;
    }

    public boolean checkIfExist(String email, String password) {
        String[] columns = {
                USER_COLUMN_ID};

        SQLiteDatabase db = this.getReadableDatabase();

        String select = USER_COLUMN_EMAIL + " = ?" + " AND " + USER_COLUMN_PASSWORD + " = ?";
        String[] selectArg = {email, password};

        Cursor cursor = db.query(USERS_TABLE,
                columns,
                select,
                selectArg,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        return cursorCount > 0;
    }

}
